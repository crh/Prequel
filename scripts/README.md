Scripts
-------

This directory contains:

* `git_utils.py`:     A utility module.
* `git_keywords.py`:  A git pre-commit script.
* Other stuff:        Other stuff.

The Python scripts are used to perform keyword substitution on program files
as they are committed to the repository, adding or updating the information
associated with specific keywords.

Prequel uses keywords in its source files, which helps us keep track of
what happened when and who did it.  Yes, git also provides that information
(and in greater detail), but if'n all you've got is a copy of a file outside
of a repository then you're out of context and out of luck.  Keyword
substitution mitigates that minor problem.

Setting up your repository to support keyword substitution:

1. Make sure `git_keywords.py` is executable.

2. In the `<repository>/.git/hooks` directory:

    `$ ln -s ../../scripts/git_keywords.py pre-commit`

3. Make sure that the `<repository>/.gitattributes` file contains something
   that looks sort of like this:

    ```
    *.c kwsub=true
    *.h kwsub=true
    *.py kwsub=true
    README.md kwsub=true
    ```

4. Make sure that your username and e'mail address are correctly assigned in
   the `<repository>/.git/config` file.
____
$Id: README.md; 2017-08-30 14:55:15 -0500; Christopher R. Hertel$
