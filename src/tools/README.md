Prequel Tools
=============

This directory provides a set of stand-alone utilities that have proven
useful in analyzing and working with PeerDist, particularly PeerDist V1.

`mcrypt_key_dx` and `oSSL_key_dx`
---------------------------------

_**Server Passphrase** and **Server Secret** Extraction_

Windows servers that support the PeerDist v1 protocol (via HTTP or SMB2)
sign the **Segment Hash of Data (HoD)** field using a **Server Secret**
to produce the **Segment Secret** for a segment of data.  The **Server
Secret** is the SHA-256 hash of the **Server Passphrase**.

Did that make any sense? No, of course not.

Don't worry, it's all documented with greater clarity in
[[MS-PCCRC]](https://msdn.microsoft.com/en-us/library/dd303704.aspx).
Note, though, that we (the Prequel geeks) introduce the new term
**“Server Passphrase”** to name what [MS-PCCRC] obliquely describes as
“an arbitrary length binary string stored on the server”.

Windows servers allow you to export both the **Server Passphrase** and
the **Server Secret** using the Netsh exportkey command for BranchCache,
as follows:

`> netsh branchcache exportkey [outputfile=] FilePath [passphrase=] PassPhrase`

The `outputfile` that is created will contain both the **Server Secret**
and the **Server Passphrase**, but the file will be AES encrypted using
the user-supplied PassPhrase.  The decryption process is briefly
described in
[[MS-PCCRC;2.5](https://msdn.microsoft.com/en-us/library/jj665526.aspx)].

The testing programs provided will decrypt the `outputfile`.  The
comments and code explain in detail how the decryption is done.  (The
code is copiously commented.)  There are two tools, one based on
`libmcrypt` and the other based on `OpenSSL`.  They work identically;
both decrypt the file that the Windows `netsh` command (shown above)
produces.  This can be useful if you want to verify that your code is
producing the same output as a Windows server, given the same input.

`pdDump`
--------

This tool will dump the Content Information of PeerDist V1 messages
returned by PeerDist-aware webservers (from Microsoft) and also the Hash
Cache files that the `prequeld` (the Prequel Daemon) produces.

`pdSizeCalc`
------------

This is a simple tool that will calculate the output size of PeerDist v1
**Content Information**, given a length and offset within a
(theoretical) input file.

`SHA256oSSL`
------------

The most common command-line tools for generating hashes will typically
produce a text string made up of hexadecimal digits.  We actually want
the hash in raw binary format.  This very simple program produces an
SHA256 sum in 32-byte binary format.

____
$Id: README.md; 2020-10-30 16:44:51 -0500; crh$
