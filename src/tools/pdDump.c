/* ========================================================================== **
 *                                  pdDump.c
 *
 * Copyright:
 *  Copyright (C) 2011, 2012 by Christopher R. Hertel
 *
 * Email: crh@ubiqx.org
 *
 * $Id: pdDump.c; 2017-09-14 12:37:50 -0500; Christopher R. Hertel$
 *
 * -------------------------------------------------------------------------- **
 *
 * Description:
 *  Dump peerdist messages in a readable format.
 *
 * -------------------------------------------------------------------------- **
 *
 * License:
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 *  This code was developed in participation
 *  with the Protocol Freedom Information Foundation.
 *  http://www.protocolfreedom.org/
 * -------------------------------------------------------------------------- **
 *
 * Notes:
 *
 *  + Compile:  cc -o pdDump pdDump.c
 *
 *  + Just a quick and dirty program to parse peerdist messages returned
 *    from peerdist-capable HTTP1.1 servers.
 *
 * ========================================================================== **
 */

#include <stdio.h>      /* Standard Input Output.   */
#include <stdlib.h>     /* Standard library stuff.  */
#include <inttypes.h>   /* Integer types & macros.  */
#include <stdarg.h>     /* Variable argument lists. */
#include <errno.h>      /* The <errno> variable.    */
#include <string.h>     /* For strerror(3).         */
#include <ctype.h>      /* For isprint(3).          */


/* -------------------------------------------------------------------------- **
 * Macros:
 *
 *  uchar   - Why isn't this standard?
 *  Err()   - Shorthand for (void)fprintf( stderr, ... )
 *  Say()   - Shorthand for (void)printf( ... )
 *            Yeah, these are kinda weenie but they're much quicker than
 *            typing out the entire function call.
 *  ErrStr  - Shorthand for the error message associated with <errno>.
 */

#define uchar      unsigned char
#define Err( ... ) (void)fprintf( stderr, __VA_ARGS__ )
#define Say( ... ) (void)printf( __VA_ARGS__ )
#define ErrStr     (strerror( errno ))


/* -------------------------------------------------------------------------- **
 * Static Functions:
 */

static void Fail( char *fmt, ... )
  /* ------------------------------------------------------------------------ **
   * Format and print a failure message on <stderr>, then exit the process.
   *
   *  Input:  fmt - Format string, as used in printf(), etc.
   *          ... - Variable parameter list.
   *
   *  Output: none
   *
   *  Notes:  Exits the process returning EXIT_FAILURE.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  va_list ap;

  va_start( ap, fmt );
  (void)fprintf( stderr, "Failure: " );
  (void)vfprintf( stderr, fmt, ap );
  va_end( ap );
  exit( EXIT_FAILURE );
  } /* Fail */


static int hashSize( const uint32_t HashID )
  /* ------------------------------------------------------------------------ **
   * Return the byte length of the hash identified by the input HashID.
   *
   *  Input:  HashID  - Numeric hash identifier.  See [MS-PCCRC].
   *
   *  Output: 32, 48, or 64 for SHA-256, SHA-384, SHA-512 respectively,
   *          or zero if <HashID> is unrecognized.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  switch( HashID )
    {
    case 0x800C:
      return( 32 );
    case 0x800D:
      return( 48 );
    case 0x800E:
      return( 64 );
    }
  return( 0 );
  } /* hashSize */


static char const *hashName( const uint32_t HashID )
  /* ------------------------------------------------------------------------ **
   * Return the name of the hash identified by the input PeerDist HashID.
   *
   *  Input:  HashID  - Numeric hash identifier.  See [MS-PCCRC].
   *
   *  Output: A pointer to a static string identifying the hash.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  switch( HashID )
    {
    case 0x800C:
      return( "SHA-256" );
    case 0x800D:
      return( "SHA-384" );
    case 0x800E:
      return( "SHA-512" );
    }
  return( "Unknown" );
  } /* hashName */


static uint16_t getInt16( FILE *inf )
  /* ------------------------------------------------------------------------ **
   * Read a 16-bit unsigned integer in Intel byte order into a uint16_t.
   *
   *  Input:  inf - Input file.
   *
   *  Output: An unsigned 16-bit integer.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  int a;
  int b;

  a = fgetc( inf );
  b = fgetc( inf );
  if( EOF == a || EOF == b )
    Fail( "Unexpected end-of-file while reading a 16-bit integer.\n" );
  return( (uint16_t)(((b & 0xFF) << 8) + (a & 0xFF)) );
  } /* getInt16 */


static uint32_t getInt32( FILE *inf )
  /* ------------------------------------------------------------------------ **
   * Read a 32-bit unsigned integer in Intel byte order into a uint32_t.
   *
   *  Input:  inf - Input file.
   *
   *  Output: An unsigned 32-bit integer.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  uint32_t val;
  int      a;
  int      i;

  for( val = i = 0; i < 4; i++ )
    {
    a = fgetc( inf );
    if( EOF == a )
      Fail( "Unexpected end-of-file while reading a 32-bit integer.\n" );
    val += (((uint32_t)(a & 0xFF)) << (8*i));
    }
  return( val );
  } /* getInt32 */


static uint64_t getInt64( FILE *inf )
  /* ------------------------------------------------------------------------ **
   * Read a 64-bit unsigned integer in Intel byte order into a uint64_t.
   *
   *  Input:  inf - Input file.
   *
   *  Output: An unsigned 64-bit integer.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  uint64_t val;
  int      a;
  int      i;

  for( val = i = 0; i < 8; i++ )
    {
    a = fgetc( inf );
    if( EOF == a )
      Fail( "Unexpected end-of-file while reading a 64-bit integer.\n" );
    val += (((uint64_t)(a & 0xff)) << (8*i));
    }
  return( val );
  } /* getInt64 */


static void dumpHash( FILE *inf, const int len )
  /* ------------------------------------------------------------------------ **
   * Raw hex dump of <len> input bytes.
   *
   *  Input:  inf - Input file.
   *          len - Number of bytes of <inf> to dump as hex strings.
   *
   *  Output: <none>
   *
   *  Notes:  If <len> is zero, this function does nothing.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  int i;
  int c;

  for( i = 0; i < len; i++ )
    {
    if( EOF == (c = fgetc( inf )) )
      Fail( "Unexpected end-of-file while reading a single byte.\n" );
    Say( "%.2x", c );
    }
  } /* dumpHash */


static void dumpPrequelHdr( FILE *inf )
  /* ------------------------------------------------------------------------ **
   * Print the PrequelD header, if there is one present.
   *
   *  Input:  inf - Input stream.
   *
   *  Output: <none>
   *
   *  Notes:  The PrequelD daemon creates PeerDist Content Information from
   *          source files.  The Content Information is stored in a cache
   *          file.  At the start of the hash cache file there is a brief
   *          header.  This function dumps the contents of that header.
   *
   *          An interesting bug was discovered while testing this function.
   *          When passing results of function calls as parameters to the
   *          Say() macro, the order of evaluation was all messed up.
   *          Example:  Say( "%d.%d\n", fgetc( inf ), fgetc( inf ) );
   *          In testing, the input from <inf> were two bytes: 0x00, 0x01.
   *          The printed result, however, was "1.0".  The order of the
   *          bytes had been reversed, suggesting that GCC evaluated the
   *          second call to fgetc() first.  The fix was:
   *            c = fgetc( inf );
   *            Say( "%d.%d\m", c, fgetc( inf ) );
   *          ...which forces correct ordering of the calls to fgetc().
   *
   * ------------------------------------------------------------------------ **
   */
  {
  uint16_t namelen;
  uint16_t offset;
  int      i;
  int      c;

  Say( "PrequelD Header\n  {\n" );
  Say( "  Fingerprint...............: \"PrequelD\"\n" );
  c = fgetc( inf );     /* See note above.  */
  Say( "  Format Version............: %d.%d\n", c, fgetc( inf ) );
  offset = getInt16( inf );
  Say( "  Content Information Offset: %d bytes\n", offset );
  Say( "  Content Information Size..: %"PRIu64" bytes\n", getInt64( inf ) );
  namelen = getInt16( inf );
  Say( "  Source File Name Length...: %d bytes\n", namelen );
  Say( "  Source File PathName......: " );
  for( i = 0; i < namelen; i++ )
    {
    c = fgetc( inf );
    if( isprint( c ) )
      (void)putchar( c );
    else
      {
      if( '\0' == c )
        Say( "\\0" );
      else
        Say( "\\x%.2x", c );
      }
    }
  putchar( '\n' );
  Say( "  Padding...................: " );
  for( i = (22 + namelen); i < offset; i++ )
      {
      c = fgetc( inf );
      if( '\0' == c )
        Say( "\\0" );
      else
        Say( "\\x%.2x", c );
      }
  Say( "\n  }\n" );
  } /* dumpPrequelHdr */


static void dumpPeerDist( FILE *inf )
  /* ------------------------------------------------------------------------ **
   * Dump what we hope is a PeerDist Content Information structure.
   *
   *  Input:  inf - Input file.
   *
   *  Output: <none>
   *
   * ------------------------------------------------------------------------ **
   */
  {
  uint16_t  i16;
  uint32_t  i32;
  uint64_t  i64;
  char      bufr[8];
  int       hashlen;
  int       segcount;
  int       i, j;

  /* Read the first 8 bytes to see if there is a PrequelD header.
   *  Reading into <bufr> causes us some headaches later on.
   */
  if( fread( bufr, 1, 8, inf ) != 8 )
    Fail( "Error reading input; %s\n", ErrStr );
  if( 0 == strncmp( "PrequelD", bufr, 8 ) )
    {
    dumpPrequelHdr( inf );
    if( fread( bufr, 1, 8, inf ) != 8 )
      Fail( "Error reading input; %s\n", ErrStr );
    }

  /* The PeerDist Content Information Header.
   *  The first 8 bytes are in <bufr>, but the rest is still in the
   *  input stream, so we have to do some fiddling.
   */
  i16 = (uint16_t)(bufr[1] << 8) + bufr[0];
  Say( "PeerDist Version...........: %d.%d (Version = 0x%.4x)\n",
       (i16>>8), (i16&0xFF), i16 );
  i32 = (((uint32_t)bufr[2]) & 0xFF)
      | ((((uint32_t)bufr[3]) & 0xFF) << 8)
      | ((((uint32_t)bufr[4]) & 0xFF) << 16)
      | ((((uint32_t)bufr[5]) & 0xFF) << 24);
  hashlen = hashSize( i32 );
  Say( "Hash Algorithm.............: %s (dwHashAlgo = 0x%.8x)\n",
       hashName( i32 ), i32 );
  i32 = ((uint32_t)(bufr[7]) << 8) + (uint32_t)bufr[6]
      + ((uint32_t)(getInt16( inf )) << 16);
  Say( "Offset into First Segment..: %d (dwOffsetInFirstSegment = 0x%.8x)\n",
       i32, i32 );
  i32 = getInt32( inf );
  Say( "Range bytes in Last Segment: %d (dwReadBytesInLastSegment = 0x%.8x)\n",
       i32, i32 );
  i32 = getInt32( inf );
  Say( "Segment count..............: %d (cSegments = 0x%.8x)\n", i32, i32 );

  /* Dump the SegmentDescription array. */
  segcount = i32;
  Say( "Segment Descriptions (%d):\n", segcount );
  for( i = 0; i < segcount; i++ )
    {
    Say( "  {\n" );
    i64 = getInt64( inf );
    Say( "  Range Start..............: %"PRIu64
         " (ullOffsetInContent = 0x%.16"PRIx64")\n", i64, i64 );
    i32 = getInt32( inf );
    Say( "  Length of Segment........: %d (cbSegment = 0x%.8x)\n", i32, i32 );
    i32 = getInt32( inf );
    Say( "  Block Size (always 64K)..: %d (cbBlockSize = 0x%.8x)\n", i32, i32 );
    Say( "  SegmentHashOfData........:\n    [" );
    dumpHash( inf, hashlen );
    Say( "]\n" );
    Say( "  SegmentSecret (HMAC).....:\n    [" );
    dumpHash( inf, hashlen );
    Say( "]\n" );
    Say( "  }\n" );
    }

  /* Dump the SegmentContentBlock array. */
  Say( "Segment Content Blocks (%d):\n", segcount );
  for( i = 0; i < segcount; i++ )
    {
    Say( "  {\n" );
    i32 = getInt32( inf );
    Say( "  Block count.............: %d (cBlocks = 0x%.8x)\n", i32, i32 );
    Say( "    {\n" );
    for( j = 0; j < i32; j++ )
      {
      Say( "    %.3d: ", j );
      dumpHash( inf, hashlen );
      Say( "\n" );
      }
    Say( "    }\n" );
    Say( "  }\n" );
    }
  } /* dumpPeerDist */


/* -------------------------------------------------------------------------- **
 * Program Mainline:
 */

int main( int argc, char *argv[] )
  /* ------------------------------------------------------------------------ **
   * Program Mainline.
   *
   *  Input:  argc  - You know what this is.
   *          argv  - You know what to do.
   *
   *  Output: EXIT_SUCCESS if we're happy, EXIT_FAILURE if we're sad.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  FILE *inf;

  /* Figure out from whence our input will arrive.
   *  Also figure out if the user needs help.
   *  FIX: This is weak... we should be using getopt(3) and "-" should
   *       mean <stdin> (in addition to no name specified).
   */
  if( 1 == argc )
    {
    Err( "[Reading from <stdin>.] (\"%s -h\" for help)\n", argv[0] );
    inf = stdin;
    }
  else
    {
    if( argc > 2 || ((2 == argc) && ('-' == argv[1][0])) )
      {
      Err( "Usage:  %s [<input_file>]\n", argv[0] );
      Err( "\tIf no <input_file> is given, the program reads" );
      Err( " from standard input.\n" );
      return( EXIT_FAILURE );
      }
    inf = fopen( argv[1], "r" );
    if( NULL == inf )
      {
      Err( "Failure opening file \"%s\"; %s\n", argv[1], ErrStr );
      return( EXIT_FAILURE );
      }
    Err( "[Reading from <%s>.]\n", argv[1] );
    }

  /* Read and parse the input file.
   */
  dumpPeerDist( inf );

  return( EXIT_SUCCESS );
  } /* main */

/* ========================================================================== */
