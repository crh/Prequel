/* ========================================================================== **
 *                                pdSizeCalc.c
 *
 * Copyright:
 *  Copyright (C) 2011, 2012 by Christopher R. Hertel
 *
 * Email: crh@ubiqx.org
 *
 * $Id: pdSizeCalc.c; 2017-09-14 12:37:50 -0500; Christopher R. Hertel$
 *
 * -------------------------------------------------------------------------- **
 *
 * Description:
 *  Calulates the size of PeerDist Content Information for the given range.
 *
 * -------------------------------------------------------------------------- **
 *
 * License:
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 *  This code was developed in participation
 *  with the Protocol Freedom Information Foundation.
 *  http://www.protocolfreedom.org/
 * -------------------------------------------------------------------------- **
 *
 * Notes:
 *
 *  To compile:
 *    cc -o pq_size_calc pq_size_calc.c
 *
 * FIX:   This uses a fixed 32-byte hash size, which is fine if the hash is
 *        SHA-256, but wrong for other available hashes.
 *
 * ========================================================================== **
 */

#include <stdlib.h>     /* Standard library.        */
#include <stdio.h>      /* Standard Input/Output.   */
#include <stdarg.h>     /* Variable argument lists. */
#include <string.h>     /* For strchr(3).           */
#include <stdint.h>     /* Standard integer types.  */


/* -------------------------------------------------------------------------- **
 * Defined Constants:
 *
 *  MAX_64K   - ...is 64KB.
 *  MAX_1M    - ...is 1MB.
 *  MAX_32M   - ...is 32MB.
 *  MAX_1G    - ...is 1GB.
 *  HASH_LEN  - Is the size of the hashes returned by the server.
 *              FIX:  The protocol allows for 32, 48, or 64 byte hash
 *                    lengths.  32-byte is the only one actually used,
 *                    at present.
 */

#define MAX_64K 0x00010000
#define MAX_1M  0x00100000
#define MAX_32M 0x02000000
#define MAX_1G  0x40000000
#define HASH_LEN 32


/* -------------------------------------------------------------------------- **
 * Macros:
 *
 *  Err()   - Shorthand for (void)fprintf( stderr, ... )
 *  Say()   - Shorthand for (void)printf( ... )
 *            Yeah, these are kinda weenie but they're much quicker than
 *            typing out the entire function call.
 */

#define Err( ... ) (void)fprintf( stderr, __VA_ARGS__ )
#define Say( ... ) (void)printf( __VA_ARGS__ )


/* -------------------------------------------------------------------------- **
 * Typedefs:
 *
 *  range - Holds the offset and length of the requested range.
 */

typedef struct
  {
  uint64_t offset;
  uint64_t length;
  } range;


/* -------------------------------------------------------------------------- **
 * Static Functions:
 */

static range parseRange( const char *s )
  /* ------------------------------------------------------------------------ **
   * Parse a range string into offset and length.
   *
   *  Input:  s - A pointer to the range string to be parsed.
   *
   *  Output: A range structure, containing both the starting offset and
   *          the length, relative to the offset, of the selected range.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  char     *pos;
  uint64_t  tmp;
  range     r;

  pos = strchr( s, '-' );
  tmp = strtoull( s, NULL, 0 );
  if( NULL == pos )
    {
    r.offset = 0;
    r.length = tmp;
    }
  else
    {
    r.offset = tmp;
    r.length = strtoull( pos+1, NULL, 0 );
    }

  return( r );
  } /* parseRange */

static uint64_t calculateSize( range r )
  /* ------------------------------------------------------------------------ **
   * Calculate the number of bytes in the content information.
   *
   *  Input:  r - The byte range, within the supposed file, that we would
   *              supposedly select if we were actually requesting a range
   *              of bytes from a file.
   *
   *  Output: The number of bytes we expect will be in the PeerDist Content
   *          Information received from the server.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  uint64_t b;
  uint64_t segcount;
  uint64_t blockcount;
  uint64_t tmp;

  /* tmp        - The offset, relative to the file start, that is one byte
   *              beyond the last byte in the range.
   * segcount   - The number of segments that encompas the range.
   * blockcount - The number of blocks that encompas the range.
   */
  tmp         = r.offset + r.length;
  segcount    = (tmp % MAX_32M) ? 1 + (tmp / MAX_32M) : (tmp / MAX_32M);
  segcount   -= (r.offset / MAX_32M);
  blockcount  = (tmp % MAX_64K) ? 1 + (tmp / MAX_64K) : (tmp / MAX_64K);
  blockcount -= (r.offset / MAX_64K);

  /* Debugging...
  Err( "offset: %lu, length: %lu\n", r.offset, r.length );
  Err( "tmp: %lu, Segments: %lu, Blocks: %lu\n", tmp, segcount, blockcount );
  */

  /* The header is 18 bytes in size.
   * Each SegmentDescription is 16 bytes plus two hashes.  Then add 4 bytes
   * for the cBlocks field in each SegmentContentBlock.
   * Finally, add in a hash for each block in the range.
   */
  b  = 18;
  b += segcount * (16 + HASH_LEN + HASH_LEN + 4);
  b += blockcount * HASH_LEN;
  return( b );
  } /* calculateSize */

/* -------------------------------------------------------------------------- **
 * Mainline:
 */

int main( int argc, char *argv[] )
  /* ------------------------------------------------------------------------ **
   * Program mainline.
   *
   *  Input:  argc  - You know what this is.
   *          argv  - You know what to do.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  uint64_t    ci_size;
  long double h_size;

  if( argc != 2 )
    {
    Err( "Usage: %s <range>\n", argv[0] );
    Err( "\tWhere <range> is either a length (representing a file size),\n" );
    Err( "\tor <offset>-<length>.\n" );
    Err( "\tNote:  The SHA256 digest is assumed.\n" );
    exit( EXIT_FAILURE );
    }

  ci_size = calculateSize( parseRange( argv[1] ) );
  Say( "Content Information size: %lu bytes", ci_size );

  h_size = 0;
  if( ci_size / MAX_1G > 0 )
    {
    h_size = ((long double)ci_size) / MAX_1G;
    Say( " (%.2Lf GB)\n", h_size );
    exit( EXIT_SUCCESS );
    }
  if( ci_size / MAX_1M > 0 )
    {
    h_size = ((long double)ci_size) / MAX_1M;
    Say( " (%.2Lf MB)\n", h_size );
    exit( EXIT_SUCCESS );
    }
  Say( "\n" );
  exit( EXIT_SUCCESS );
  } /* main */

/* ========================================================================== */
