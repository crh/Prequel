Utilities
=========

This is the junk drawer.  General-purpose code goes in here.

HexOct
------

Provides a set of functions for converting between coder-readable
hexadecimal and octal notation and raw binary data.  This includes
generating a simple hex dump of arbitrary binary data.

Hex dump test code can be compiled as follows:

```cc -DHEXDUMPMAIN -o HexDump HexOct.c```

The result is a simple program that will provide a hexdump of an input file,
or via `stdin`, pretty much as you'd expect.  This test code also serves as
example code.

____
$Id: README.md; 2020-09-30 10:51:23 -0500; crh$
